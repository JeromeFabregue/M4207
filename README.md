Resume séance 03/02/17

J'ai travaillé sur le GPS mais en analysant les données en sortie, il se trouve que les données sont erronées et donc inexploitable dans le cadre du projet.
J'ai ensuite essayer de récolter des données sur les réseaux Wi-Fi sans succès. Je peux connecter l'arduino à une AP(via l'exemple WifiWebClient)  mais les exemples qui permettent
de récupérer des données sur l'AP ne retourne aucun résultat.

La prochaine étape serait d'arriver à produire des données GPS exploitables ou pouvoir récupérer les adresses MAC des AP afin de pouvoir déterminer la position de la clé en 
fonction de l'AP le plus proche de la clé.

Résumé séance 14/02/17

J'ai travaillé sur l'affichage du niveau de la batterie et de la charge sur l'écran LCD. La couleur varie en fonction du niveau de batterie.
Le code est disponible dans le fichier lcd.ino sur mon dépôt.

La prochaine étape serait de réussir la localisation ou de trouver un moyen d'afficher le nom de la personne qui a prit la clé en utilisant un système pour authentifier cette 
personne.
