/*Brancher l'écran sur le port I2C */
#include <Wire.h>
#include <LBattery.h>
#include "rgb_lcd.h"
rgb_lcd lcd;

#define LED 2 //connect LED to digital pin2
void setup() {
    // set up the LCD's number of columns and rows:
    lcd.begin(16, 2);
}
void loop() {

  
  lcd.setCursor(0,0);
  lcd.print("Battery % = ");
  lcd.print(LBattery.level());
  lcd.print("%");
  lcd.setCursor(0,1);
  if (LBattery.isCharging() == 1){
    lcd.print("Charging = YES" );
  }
  else {
    lcd.print("Charging = NO" );
  }
  if(LBattery.level()<=100) {
          // Turn on the display:
    lcd.display();
    lcd.setRGB(0,255,0);
  }
   if(LBattery.level()<=66) {
    lcd.display();
    lcd.setRGB(255,128,0);
  }
  if(LBattery.level()<=33) {
    lcd.display();
    lcd.setRGB(155,0,0);
  }
}